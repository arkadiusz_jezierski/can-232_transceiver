/* 
 * File:   board.h
 * Author: Jezierski
 *
 * Created on 30 maj 2013, 19:56
 */

#ifndef BOARD_H
#define	BOARD_H

#include <pic18.h>
#include "delay.h"

#define LED_FUN_DIR    TRISA
#define LED_FUN_PORT   LATA
#define LED_FUN_1   2
#define LED_FUN_2   1
#define LED_FUN_3   0

#define LED_CANERR_DIR   TRISA
#define LED_CANERR_PORT   LATA
#define LED_CANERR_ACT  3
#define LED_CANERR_PASS  4
#define LED_CANERR_OFF  5

#define LED_UART_DIR    TRISC
#define LED_UART_PORT   LATC
#define LED_UART_RX     3
#define LED_UART_TX     2

#define LED_CAN_DIR    TRISC
#define LED_CAN_PORT   LATC
#define LED_CAN_RX     5
#define LED_CAN_TX     4

#define LED_CANERR_ACT_ON  LED_CANERR_PORT |= (1 << LED_CANERR_ACT);
#define LED_CANERR_PASS_ON  LED_CANERR_PORT |= (1 << LED_CANERR_PASS);
#define LED_CANERR_OFF_ON  LED_CANERR_PORT |= (1 << LED_CANERR_OFF);
#define LED_CANERR_ACT_OFF  LED_CANERR_PORT &= ~(1 << LED_CANERR_ACT);
#define LED_CANERR_PASS_OFF  LED_CANERR_PORT &= ~(1 << LED_CANERR_PASS);
#define LED_CANERR_OFF_OFF  LED_CANERR_PORT &= ~(1 << LED_CANERR_OFF);

#define LED_FUN_1_ON    LED_FUN_PORT |= (1 << LED_FUN_1);
#define LED_FUN_2_ON    LED_FUN_PORT |= (1 << LED_FUN_2);
#define LED_FUN_3_ON    LED_FUN_PORT |= (1 << LED_FUN_3);
#define LED_FUN_1_OFF    LED_FUN_PORT &= ~(1 << LED_FUN_1);
#define LED_FUN_2_OFF    LED_FUN_PORT &= ~(1 << LED_FUN_2);
#define LED_FUN_3_OFF    LED_FUN_PORT &= ~(1 << LED_FUN_3);

#define LED_CANRX_ON    LED_CAN_PORT |= (1 << LED_CAN_RX);
#define LED_CANTX_ON    LED_CAN_PORT |= (1 << LED_CAN_TX);
#define LED_CANRX_OFF    LED_CAN_PORT &= ~(1 << LED_CAN_RX);
#define LED_CANTX_OFF    LED_CAN_PORT &= ~(1 << LED_CAN_TX);
#define LED_CANRX_TG    LED_CAN_PORT ^= (1 << LED_CAN_RX);
#define LED_CANTX_TG    LED_CAN_PORT ^= (1 << LED_CAN_TX);

#define LED_UARTRX_ON    LED_UART_PORT |= (1 << LED_UART_RX);
#define LED_UARTTX_ON    LED_UART_PORT |= (1 << LED_UART_TX);
#define LED_UARTRX_OFF    LED_UART_PORT &= ~(1 << LED_UART_RX);
#define LED_UARTTX_OFF    LED_UART_PORT &= ~(1 << LED_UART_TX);
#define LED_UARTRX_TG    LED_UART_PORT ^= (1 << LED_UART_RX);
#define LED_UARTTX_TG    LED_UART_PORT ^= (1 << LED_UART_TX);

void initPortIO(void);
void testPortIO(void);
void setCanErrorLeds(void);

#endif	/* BOARD_H */

