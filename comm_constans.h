/*
 * File:   comm_constans.h
 * Author: jezierski
 *
 * Created on November 25, 2014, 7:33 PM
 */

#ifndef COMM_CONSTANS_H
#define	COMM_CONSTANS_H

//************ UART-CAN transceiver constans *******************//
#define CMD_REQ_INIT        'i'
#define CMD_REQ_RESET       'r'
#define CMD_REQ_HNDSHK      'h'
#define CMD_REQ_MASK        'm'
#define CMD_REQ_FILT        'f'
#define CMD_REQ_GET         'g'
#define CMD_REQ_TRANS       't'
#define CMD_REQ_TRANS_REG   'y'
#define CMD_REQ_DELETE      'd'

#define CMD_RESP_DATA        'D'
#define CMD_RESP_ACK         'A'
#define CMD_RESP_NACK        'X'
#define CMD_RESP_BAD_CRC     'B'
#define CMD_RESP_NO_DATA     'N'
#define CMD_RESP_ERROR       'E'
#define CMD_RESP_HNDSHK      'H'

#define REQ_FRAME_HDR       '?'
#define RESP_FRAME_HDR       'T'
#define REQ_FRAME_FTR       CR

#define FRAME_UART2CAN_MIN_SIZE       9 // [Hdr] [Len] [Cmd] [Id] [Id] [Id] [Id] [Crc] [CR]
#define OFFSET_UART2CAN_HEADER   0
#define OFFSET_UART2CAN_LENGTH   1
#define OFFSET_UART2CAN_COMMAND  2
#define OFFSET_UART2CAN_ID       3
#define OFFSET_UART2CAN_DATA     7
#define OFFSET_UART2CAN_NO       3
#define OFFSET_UART2CAN_MODE     3
#define OFFSET_UART2CAN_FILT_ID  4

#define OFFSET_RESP_RXERR    3
#define OFFSET_RESP_TXERR    4
#define OFFSET_RESP_COMSTAT  5
#define OFFSET_RESP_TXCON    6
#define OFFSET_RESP_TXBIE    5
#define OFFSET_RESP_BIE0     6
#define OFFSET_RESP_RFIFO_SIZE     7
#define OFFSET_RESP_WFIFO_SIZE     8

#define CR      13

#define MODE_0  0
#define MODE_1  1
//************ UART-CAN transceiver constans *******************//

//************************ BOOT CONSTANS ***********************//
#define BOOT_PUT_CMD                    0
#define BOOT_PUT_DATA                   1
#define BOOT_GET_CMD                    2
#define BOOT_GET_DATA                   3

#define BOOT_WRITE_UNLOCK               1
#define BOOT_ERASE_ONLY                 2
#define BOOT_AUTO_ERASE                 4
#define BOOT_AUTO_INC                   8
#define BOOT_SEND_ACK                   16

#define BOOT_CMD_RESET                  1
#define BOOT_CMD_INIT_CHK               2
#define BOOT_CMD_CHK_RUN                3
#define BOOT_CMD_EXIT_BOOT              4
#define BOOT_CMD_CLR_PROG               5
#define BOOT_CMD_GET_UID               6
#define BOOT_CMD_SET_UID               7
#define BOOT_CMD_GET_READY             8
#define BOOT_CMD_INIT_READ             9
#define BOOT_CMD_INIT_WRITE             10

#define BOOT_COMMAND_ACK                0x88
//**************************************************************//

//*************** CAN-BUS drivers commands *********************//

//**    Global Commands **//
#define CMD_CAN_GLOB_BOOT       1
#define CMD_CAN_GLOB_INIT       2
#define CMD_CAN_GLOB_ERR_ACK    3
//************************//

//**    Common Commands **//
#define CMD_CAN_DUMMY    0
#define CMD_CAN_ACK     1
#define CMD_CAN_NACK     8
#define CMD_CAN_PING     2
#define CMD_CAN_QUIET     4
#define CMD_CAN_ALL_OFF     5
#define CMD_CAN_CHECK       6
#define CMD_CAN_PONG       9
//************************//


//**    PWM Actutor Commands **//
#define CMD_CAN_SET_PWM         30
#define CMD_CAN_GET_PWM         31
#define CMD_CAN_PWM_UP          32
#define CMD_CAN_PWM_DOWN        33
#define CMD_CAN_SET_PWM_ALL     34
#define CMD_CAN_SET_PWM_ALL_THE_SAME     35
#define CMD_CAN_PWM_UP_ALL      36
#define CMD_CAN_PWM_DOWN_ALL    37
//****************************************//


//**    RGB Actutor Commands **//
#define CMD_CAN_SET_RGB_CHANNEL_RED         30
#define CMD_CAN_SET_RGB_CHANNEL_GREEN         31
#define CMD_CAN_SET_RGB_CHANNEL_BLUE         32
#define CMD_CAN_SET_RGB_CHANNEL_ALL         33
#define CMD_CAN_SET_RGB_MODE         34
#define CMD_CAN_SET_RGB_SPEED         35
#define CMD_CAN_GET_RGB_CHANNEL_RED         36
#define CMD_CAN_GET_RGB_CHANNEL_GREEN         37
#define CMD_CAN_GET_RGB_CHANNEL_BLUE         38
#define CMD_CAN_GET_RGB_CHANNEL_ALL         39
#define CMD_CAN_GET_RGB_MODE         40
#define CMD_CAN_GET_RGB_SPEED         41
//****************************************//

//**    Bistable Switch Actuator Commands **//
#define CMD_CAN_SET_OUTPUT     30
#define CMD_CAN_GET_OUTPUT_STATE     31
//******************************************//

//**    Bistable Switch Sensor Commands **//
#define CMD_CAN_GET_SENSOR_STATUS     30
//****************************************//


//*************** CAN-BUS drivers commands **********************//


//*************** CAN-BUS masks and filters *********************//
#define SLAVE_INIT_MASK_0               0x0fffffff
#define SLAVE_INIT_MASK_1               0x10ffffff
#define SLAVE_INIT_ADR_FILTER           0x09000000
#define SLAVE_GLOBAL_BROADCAST_FILTER   0x10000000
#define SLAVE_MASK_0                    0x1fc077ff
#define SLAVE_MASK_1                    0x10000000
#define SLAVE_FILTER                    0x000007ff
#define SLAVE_FILTER_BROADCAST          0x000007ff

#define MASTER_MASK_0                   0x1f000000
#define MASTER_MASK_1                   0x1ffff800
#define MASTER_FILTER_0                 0x08000000  //for MASK0
#define MASTER_FILTER_1                 0x04000000  //for MASK0
#define MASTER_FILTER_N                 0x003ff800  //for MASK1

#define MASTER_MASK_0_CAN_A             0xc8
#define MASTER_MASK_1_CAN_A             0xc8
#define MASTER_FILTER_0_CAN_A           0xc8
#define MASTER_FILTER_1_CAN_A           0xc8
#define MASTER_FILTER_2_CAN_A           0xc8
#define MASTER_FILTER_3_CAN_A           0xc8
#define MASTER_FILTER_4_CAN_A           0xc8
#define MASTER_FILTER_5_CAN_A           0xc8
//*************** CAN-BUS masks and filters *********************//

//********************** CAN-BUS frame **************************//
#define ID_REQ_ADDRESS                          0x08000000
#define ID_RES_ADDRESS                          0x09000000
#define ID_REQ_ERROR_INFO                       0x04000000
#define ID_GLOBAL_BROADCAST                     0x10000000
#define ID_CATEGORY_BROADCAST                   0x00000000
#define ID_ADDRESS_BROADCAST                    0x00000000
#define ID_ADDRESS_MASTER_DESTINATION           0x003ff800
#define ID_ADDRESS_MASTER_SOURCE                0x000007ff

#define CMD_ADDRESSING_COMPLETED                0x000000ff

#define OFFSET_CAN_COMMAND      0
#define OFFSET_CAN_REQ_ADR_FRAME_ID      0
#define OFFSET_CAN_REQ_ADDRESS      1
#define OFFSET_CAN_RESET_COUNTERS      1
#define OFFSET_CAN_ACK_FOR_COMMAND      1
#define OFFSET_CAN_GLOBAL_COMMAND      24
#define OFFSET_CAN_GLOBAL_BIT      28
#define OFFSET_CAN_DATA         1
#define OFFSET_CAN_ERR_BIE0           0
#define OFFSET_CAN_ERR_RXERRCNT       1
#define OFFSET_CAN_ERR_TXERRCNT       2
#define OFFSET_CAN_ERR_COMSTAT        3
#define OFFSET_CAN_ERR_TXB0B1CON      4
#define OFFSET_CAN_ERR_TXB2BIE        5
#define OFFSET_CAN_ERR_B4CON          6
#define OFFSET_CAN_ERR_B5CON          7

#define OFFSET_CAN_OUTPUT_STAT    1
#define OFFSET_CAN_INPUT_STAT    1
#define OFFSET_CAN_UID    1

#define OFFSET_CAN_PWM_VALUE    1

#define OFFSET_CAN_RGB_VALUE    1

#define MASK_FOR_UID_IN_ID      0xffffff
#define REQUESTED_ADR_QNTY      7

//********************** CAN-BUS frame **************************//

//******************** CAN-BUS categories ***********************//
#define CATEGORY_TYPE_SENSOR    0
#define CATEGORY_TYPE_ACTUATOR    1

#define CATEGORY_SENSOR_BISTABLE_SWITCH         2

#define CATEGORY_ACTUATOR_BISTABLE_SWITCH       3
#define CATEGORY_ACTUATOR_PWM                   5
#define CATEGORY_ACTUATOR_RGB                   7
//******************** CAN-BUS categories ***********************//
#endif	/* COMM_CONSTANS_H */
