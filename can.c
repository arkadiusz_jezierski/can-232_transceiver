/*********************************************************************
 *
 *   This file contains CAN drivers for PIC18Fxx8 devices
 *   
 *********************************************************************
 * FileName:        CAN.C
 * Dependencies:    CAN.H, PIC18.H or P18CXXX.H
 * Processor:       PIC18FXX8
 * Compiler:        MCC18 v2.20 or higher
 *                  HITECH PICC-18 v8.20PL4 or higher
 * Linker:          
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") is intended and supplied to you, the Company�s
 * customer, for use solely and exclusively with products manufactured
 * by the Company. 
 *
 * The software is owned by the Company and/or its supplier, and is 
 * protected under applicable copyright laws. All rights are reserved. 
 * Any use in violation of the foregoing restrictions may subject the 
 * user to criminal sanctions under applicable laws, as well as to 
 * civil liability for the breach of the terms and conditions of this 
 * license.
 
 * THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES, 
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED 
 * TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT, 
 * IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR 
 * CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 *
 *
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Thomas Castmo        06/07/2003  Initial release
 * Thomas Castmo        07/07/2003  Cleared up a bit and implemented 
 *                                  the window function CANCON<3:1> 
 *                                  for interrupts
 * Thomas Castmo        16/07/2003  Added support for the Microchip
 *                                  MPLAB C18 compiler
 * 
 ********************************************************************/
#include "can.h"
#include "uart.h"





#define CAN_INT_BITS 0x3F	//CAN interrupts which should be enabled, simply what PIE3 is to be loaded with (Note all TXB IE will be set regardless of this value)

union RXBuffer { //Receive buffer structure
    unsigned char Data[14]; //It has to be a bit messy/strict for

    struct { //it to go trough the PICC18 compiler

        union {
            unsigned char Byte;

            struct {
                unsigned FILHIT0 : 1;
                unsigned JTOFF : 1;
                unsigned RXB0DBEN : 1;
                unsigned RXRTRRO : 1;
                unsigned : 1;
                unsigned RXM0 : 1;
                unsigned RXM1 : 1;
                unsigned RXFUL : 1;
            } Bits;
        } RXBCON;

        union {
            unsigned char Byte;
        } RXBSIDH;

        union {
            unsigned char Byte;

            struct {
                unsigned EID16 : 1;
                unsigned EID17 : 1;
                unsigned : 1;
                unsigned EXID : 1;
                unsigned SRR : 1;
                unsigned SID0 : 1;
                unsigned SID1 : 1;
                unsigned SID2 : 1;
            } Bits;
        } RXBSIDL;

        union {
            unsigned char Byte;
        } RXBEIDH;

        union {
            unsigned char Byte;
        } RXBEIDL;

        union {
            unsigned char Byte;

            struct {
                unsigned DLC0 : 1;
                unsigned DLC1 : 1;
                unsigned DLC2 : 1;
                unsigned DLC3 : 1;
                unsigned RB0 : 1;
                unsigned RB1 : 1;
                unsigned RXRTR : 1;
                unsigned : 1;
            } Bits;
        } RXBDLC;

        union {
            unsigned char Array[8];

            struct {
                unsigned char RXBD0;
                unsigned char RXBD1;
                unsigned char RXBD2;
                unsigned char RXBD3;
                unsigned char RXBD4;
                unsigned char RXBD5;
                unsigned char RXBD6;
                unsigned char RXBD7;
            } Bytes;
        } RXBD;
    } Specific;
};

union TXBuffer { //Transmit buffer structure
    unsigned char Data[14];

    struct {

        union {
            unsigned char Byte;

            struct {
                unsigned TXPRI0 : 1;
                unsigned TXPRI1 : 1;
                unsigned : 1;
                unsigned TXREQ : 1;
                unsigned TXERR : 1;
                unsigned TXLARB : 1;
                unsigned TXABT : 1;
            } Bits;
        } TXBCON;

        union {
            unsigned char Byte;
        } TXBSIDH;

        union {
            unsigned char Byte;

            struct {
                unsigned EID16 : 1;
                unsigned EID17 : 1;
                unsigned : 1;
                unsigned EXIDE : 1;
                unsigned : 1;
                unsigned SID0 : 1;
                unsigned SID1 : 1;
                unsigned SID2 : 1;
            } Bits;
        } TXBSIDL;

        union {
            unsigned char Byte;
        } TXBEIDH;

        union {
            unsigned char Byte;
        } TXBEIDL;

        union {
            unsigned char Byte;

            struct {
                unsigned DLC0 : 1;
                unsigned DLC1 : 1;
                unsigned DLC2 : 1;
                unsigned DLC3 : 1;
                unsigned : 1;
                unsigned : 1;
                unsigned TXRTR : 1;
                unsigned : 1;
            } Bits;
        } TXBDLC;

        union {
            unsigned char Array[8];

            struct {
                unsigned char TXBD0;
                unsigned char TXBD1;
                unsigned char TXBD2;
                unsigned char TXBD3;
                unsigned char TXBD4;
                unsigned char TXBD5;
                unsigned char TXBD6;
                unsigned char TXBD7;
            } Bytes;
        } TXBD;
    } Specific;
};
union RXBuffer RXMessage[RXBUF]; //Received messages FIFO buffer
union TXBuffer TXMessage[TXBUF]; //Pending messages to transmit FIFO buffer

/*********************************************************************
 * Function:        void ECANInitialize(void)
 *
 * Overview:        Use this function to initialize ECAN module with
 *                  options defined in ECAN.def file.
 *                  You may manually edit ECAN.def file as per your
 *                  requirements, or use Microchip Application
 *                  Maestro tool.
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    All pending transmissions are aborted.
 ********************************************************************/
void ECANInitialize(unsigned char mode) {

    // Put module into Configuration mode.
    ECANSetOperationMode(ECAN_OP_MODE_CONFIG);


    // Set Bit rate values as per defines.
    BRGCON1 = ((ECAN_SJW_VAL - 1) << 6) | (ECAN_BRP_VAL - 1);
    BRGCON2 = (ECAN_PHSEG2_MODE_VAL << 7) |                                     \
              (ECAN_BUS_SAMPLE_MODE_VAL << 6) |                                     \
              ((ECAN_PHSEG1_VAL - 1) << 3) |                                     \
              (ECAN_PROPSEG_VAL - 1);
    BRGCON3 = (ECAN_WAKEUP_MODE_VAL << 7) |
            (ECAN_FILTER_MODE_VAL << 6) |
            (ECAN_PHSEG2_VAL - 1);

    // Set CANTX2, TXDRIVE and CAN Capture modes.
    CIOCON = ECAN_TX2_SOURCE_VAL << 7 |                                     \
             ECAN_TX2_MODE_VAL << 6 |                                     \
             ECAN_TXDRIVE_MODE_VAL << 4 |                                     \
             ECAN_CAPTURE_MODE_VAL;


    // Set WCAN functional mode.
    if (mode == ECAN_MODE_0) {
        ECANCON = ECAN_MODE_0;
    } else {
        ECANCON = ECAN_MODE_1;
    }


    // Set RXB0 and RXB1 buffer modes.
    if (mode == ECAN_MODE_0) {
        RXB0CON = (ECAN_RECEIVE_STANDARD << 5) | (ECAN_RXB0_DBL_BUFFER_MODE_VAL << 2);
        RXB1CON = ECAN_RECEIVE_STANDARD << 5;
    } else {
        // In Mode1 & 2, Map 2-bit RXM bits into 1 RXM1 bit.
#if ( ECAN_RXB0_MODE_VAL == ECAN_RECEIVE_ALL )
        RXB0CON = 0x40;
#else
        RXB0CON = 0;
#endif

#if ( ECAN_RXB1_MODE_VAL == ECAN_RECEIVE_ALL )
        RXB1CON = 0x40;
#else
        RXB1CON = 0;
#endif

        // B0-B5 are available in Mode 1 and 2 only.

#if (ECAN_B0_TXRX_MODE_VAL != ECAN_BUFFER_TX)
#if ( ECAN_B0_MODE_VAL == ECAN_RECEIVE_ALL )
        B0CON = 0x40;
#else
        B0CON = 0;
#endif
#else
        B0CON = ECAN_B0_AUTORTR_MODE << 2;
#endif

#if (ECAN_B1_TXRX_MODE_VAL != ECAN_BUFFER_TX)
#if ( ECAN_B1_MODE_VAL == ECAN_RECEIVE_ALL )
        B1CON = 0x40;
#else
        B1CON = 0;
#endif
#else
        B1CON = ECAN_B1_AUTORTR_MODE << 2;
#endif

#if (ECAN_B2_TXRX_MODE_VAL != ECAN_BUFFER_TX)
#if ( ECAN_B2_MODE_VAL == ECAN_RECEIVE_ALL )
        B2CON = 0x40;
#else
        B2CON = 0;
#endif
#else
        B2CON = ECAN_B2_AUTORTR_MODE << 2;
#endif

#if (ECAN_B3_TXRX_MODE_VAL != ECAN_BUFFER_TX)
#if ( ECAN_B3_MODE_VAL == ECAN_RECEIVE_ALL )
        B3CON = 0x40;
#else
        B3CON = 0;
#endif
#else
        B3CON = ECAN_B3_AUTORTR_MODE << 2;
#endif

#if (ECAN_B4_TXRX_MODE_VAL != ECAN_BUFFER_TX)
#if ( ECAN_B4_MODE_VAL == ECAN_RECEIVE_ALL )
        B4CON = 0x40;
#else
        B4CON = 0;
#endif
#else
        B4CON = ECAN_B4_AUTORTR_MODE << 2;
#endif

#if (ECAN_B5_TXRX_MODE_VAL != ECAN_BUFFER_TX)
#if ( ECAN_B5_MODE_VAL == ECAN_RECEIVE_ALL )
        B5CON = 0x40;
#else
        B5CON = 0;
#endif
#else
        B5CON = ECAN_B5_AUTORTR_MODE << 2;
#endif

        // Enable/disable buffers B0-B5.
        BSEL0 = ECAN_B5_TXRX_MODE_VAL << 7 |                                     \
            ECAN_B4_TXRX_MODE_VAL << 6 |                                     \
            ECAN_B3_TXRX_MODE_VAL << 5 |                                     \
            ECAN_B2_TXRX_MODE_VAL << 4 |                                     \
            ECAN_B1_TXRX_MODE_VAL << 3 |                                     \
            ECAN_B0_TXRX_MODE_VAL << 2;


    }



    // Enable/Disable filters in Modes 1 and 2 only.
    if (mode != ECAN_MODE_0) {
        RXFCON0 = (ECAN_RXF7_MODE_VAL << 7) |                                     \
              (ECAN_RXF6_MODE_VAL << 6) |                                     \
              (ECAN_RXF5_MODE_VAL << 5) |                                     \
              (ECAN_RXF4_MODE_VAL << 4) |                                     \
              (ECAN_RXF3_MODE_VAL << 3) |                                     \
              (ECAN_RXF2_MODE_VAL << 2) |                                     \
              (ECAN_RXF1_MODE_VAL << 1) |                                     \
              (ECAN_RXF0_MODE_VAL);
        RXFCON1 = (ECAN_RXF15_MODE_VAL << 7) |                                     \
              (ECAN_RXF14_MODE_VAL << 6) |                                     \
              (ECAN_RXF13_MODE_VAL << 5) |                                     \
              (ECAN_RXF12_MODE_VAL << 4) |                                     \
              (ECAN_RXF11_MODE_VAL << 3) |                                     \
              (ECAN_RXF10_MODE_VAL << 2) |                                     \
              (ECAN_RXF9_MODE_VAL << 1) |                                     \
              (ECAN_RXF8_MODE_VAL);



        // Link each filter to corresponding buffer only if we are in Mode 1 or 2.

        ECANLinkRXF0F1ToBuffer(ECAN_RXF0_BUFFER_VAL, ECAN_RXF1_BUFFER_VAL);
        ECANLinkRXF2F3ToBuffer(ECAN_RXF2_BUFFER_VAL, ECAN_RXF3_BUFFER_VAL);
        ECANLinkRXF4F5ToBuffer(ECAN_RXF4_BUFFER_VAL, ECAN_RXF5_BUFFER_VAL);
        ECANLinkRXF6F7ToBuffer(ECAN_RXF6_BUFFER_VAL, ECAN_RXF7_BUFFER_VAL);
        ECANLinkRXF8F9ToBuffer(ECAN_RXF8_BUFFER_VAL, ECAN_RXF9_BUFFER_VAL);
        ECANLinkRXF10F11ToBuffer(ECAN_RXF10_BUFFER_VAL, ECAN_RXF11_BUFFER_VAL);
        ECANLinkRXF12F13ToBuffer(ECAN_RXF12_BUFFER_VAL, ECAN_RXF13_BUFFER_VAL);
        ECANLinkRXF14F15ToBuffer(ECAN_RXF14_BUFFER_VAL, ECAN_RXF15_BUFFER_VAL);

        ECANLinkRXF0Thru3ToMask(ECAN_RXF0_MASK_VAL,                                     \
                           ECAN_RXF1_MASK_VAL,                                     \
                           ECAN_RXF2_MASK_VAL,                                     \
                           ECAN_RXF3_MASK_VAL);
        ECANLinkRXF4Thru7ToMask(ECAN_RXF4_MASK_VAL,                                     \
                           ECAN_RXF5_MASK_VAL,                                     \
                           ECAN_RXF6_MASK_VAL,                                     \
                           ECAN_RXF7_MASK_VAL);
        ECANLinkRXF8Thru11ToMask(ECAN_RXF8_MASK_VAL,                                     \
                           ECAN_RXF9_MASK_VAL,                                     \
                           ECAN_RXF10_MASK_VAL,                                     \
                           ECAN_RXF11_MASK_VAL);
        ECANLinkRXF12Thru15ToMask(ECAN_RXF12_MASK_VAL,                                     \
                           ECAN_RXF13_MASK_VAL,                                     \
                           ECAN_RXF14_MASK_VAL,                                     \
                           ECAN_RXF15_MASK_VAL);

    }

    if (mode == ECAN_MODE_0) {
        RXM0SIDL_EXIDEN = 0;
        RXM1SIDL_EXIDEN = 0;
        RXF0SIDLbits.EXIDEN = 0;
        RXF1SIDLbits.EXIDEN = 0;
        RXF2SIDLbits.EXIDEN = 0;
        RXF3SIDLbits.EXIDEN = 0;
        RXF4SIDLbits.EXIDEN = 0;
        RXF5SIDLbits.EXIDEN = 0;
    } else {
        RXM0SIDL_EXIDEN = 1;
        RXM1SIDL_EXIDEN = 1;
        RXF0SIDLbits.EXIDEN = 1;
        RXF1SIDLbits.EXIDEN = 1;
        RXF2SIDLbits.EXIDEN = 1;
        RXF3SIDLbits.EXIDEN = 1;
        RXF4SIDLbits.EXIDEN = 1;
        RXF5SIDLbits.EXIDEN = 1;
    }



    IPR3 = 0xFF;

    // Exit with speicfied mode.  If selected mode is Configuration,
    // we do not need to do anything.
#if ( ECAN_INIT_MODE != ECAN_INIT_CONFIGURATION )
    ECANSetOperationMode(ECAN_INIT_MODE);
#endif

    if (mode == ECAN_MODE_0) {
        PIE3 = CAN_INT_BITS & 0xE3; //Enable CAN interrupts except TX interrupts
    } else {
        PIE3 = CAN_INT_BITS & 0xE2;
        PIE3bits.TXB2IE = 1;
        TXBIE = 0;
        BIE0 = 0xff;

#if (ECAN_B0_TXRX_MODE_VAL == ECAN_BUFFER_TX)
        BIE0bits.B0IE = 0;
#endif
#if (ECAN_B1_TXRX_MODE_VAL == ECAN_BUFFER_TX)
        BIE0bits.B1IE = 0;
#endif
#if (ECAN_B2_TXRX_MODE_VAL == ECAN_BUFFER_TX)
        BIE0bits.B2IE = 0;
#endif
#if (ECAN_B3_TXRX_MODE_VAL == ECAN_BUFFER_TX)
        BIE0bits.B3IE = 0;
#endif
#if (ECAN_B4_TXRX_MODE_VAL == ECAN_BUFFER_TX)
        BIE0bits.B4IE = 0;
#endif
#if (ECAN_B5_TXRX_MODE_VAL == ECAN_BUFFER_TX)
        BIE0bits.B5IE = 0;
#endif
    }



    PIR3 = 0; //x18;
}

/*********************************************************************
 * Function:        void CANGetMessage(void)
 *
 * PreCondition:    <WIN2:WIN0> in the CANCON register has to set
 *                  to reflect the desired RXB registers
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    Will modify the RX FIFO Write pointer (RXWPtr)
 *
 * Overview:        Gets the registers for a RXB and puts them in the
 *                  CAN Receive buffer
 *
 * Note:            Care is not taken if buffer is full
 ********************************************************************/
void CANGetMessage() {
    unsigned char i;
    if (++RXWPtr >= RXBUF) //If pointer overflowed
        RXWPtr = 0; //Clear it

    for (i = 0; i < 14; i++) {
        RXMessage[RXWPtr].Data[i] = *(unsigned char *) (0xF60 + i);
    }
    RXB0FUL = 0;
}

/*********************************************************************
 * Function:        char CANPutMessage(void)
 *
 * PreCondition:    <WIN2:WIN0> in the CANCON register has to set
 *                  to reflect the desired TXB registers
 *
 * Input:           None
 *
 * Output:          0 -> A new message has been put in the transmit queue
 *                  1 -> There was no messages in the TX buffer to send
 *
 * Side Effects:    Will modify the TX buffer�s Read pointer (TXRPtr)
 *
 * Overview:        Checks if there is any messages to transmit and if so
 *                  place it in the registers reflected by <WIN2:WIN0> 
 *
 * Note:            None
 ********************************************************************/
char CANPutMessage(void) {
    unsigned char i;
    if (TXWPtr != TXRPtr) //If there are any more data to send
    {
        if (++TXRPtr >= TXBUF) //then increment the TX read pointer
            TXRPtr = 0;
        for (i = 0; i < 14; i++)
            *(unsigned char *) (0xF60 + i) = TXMessage[TXRPtr].Data[i];
        RXB0RTRR0 = 1;
        return 0;
    } else
        return 1;
}

/*********************************************************************
 * Function:        char CANPut(struct CANMessage Message)
 *
 * PreCondition:    None
 *
 * Input:           A CAN message
 *
 * Output:          1 -> Failed to put a CAN on the buffer, buffer is full
 *                  2 -> All transmit ECAN buffers are full
 *                  0 -> The CAN message is put on the buffer and will be 
 *                       transmitted eventually 
 *
 * Side Effects:    Will modify the TX Buffer register�s Write pointer
 *
 * Overview:        Initially checks if at least one buffer slot is available
 *                  and if so push the requested message in the buffer.
 *                  Checks if the TX modules are idle and if they are, reactivate one.
 *
 * Note:            None
 ********************************************************************/
char CANPutToFIFO(struct CANMessage Message) {
    unsigned char TempPtr, i;
    if (TXWPtr == TXRPtr - 1 || (TXWPtr == TXBUF - 1 && TXRPtr == 0)) { //if all transmit buffers are full return 1
        return 1;
    }

    //Do not modify the TX pointer until the message has been successfully copied so the CANISR don't
    //send a message that isn't ready
    if (TXWPtr >= TXBUF - 1) //check if transmit write pointer will overflow
    {
        TempPtr = 0; //and post clear write pointer
    } else {
        TempPtr = TXWPtr + 1; //and postincrement write pointer
    }

    if (Message.NoOfBytes > 8) //Ensure we don't handle more than 8 bytes
        Message.NoOfBytes = 8;

    TXMessage[TempPtr].Specific.TXBDLC.Byte = Message.NoOfBytes; //Set the data count

    if (!Message.Remote) //If dataframe
    {
        TXMessage[TempPtr].Specific.TXBDLC.Bits.TXRTR = 0; //Clear the Remote Transfer Request bit

        for (i = 0; i < Message.NoOfBytes; i++) //Load data registers
        {
            TXMessage[TempPtr].Specific.TXBD.Array[i] = Message.Data[i];
        }
    } else //Remote frame
    {
        TXMessage[TempPtr].Specific.TXBDLC.Bits.TXRTR = 1; //Set TXRTR bit
    }
    if (Message.Ext) //Extended identifier
    {
        TXMessage[TempPtr].Specific.TXBEIDL.Byte = (unsigned char) (Message.Address & 0xFF); // Put address <7:0> in EIDL
        TXMessage[TempPtr].Specific.TXBEIDH.Byte = (unsigned char) ((Message.Address >> 8) & 0xFF); // Put address <15:8> in EIDH
        TXMessage[TempPtr].Specific.TXBSIDL.Byte = (unsigned char) ((Message.Address >> 16) & 0x03) | (unsigned char) ((Message.Address >> 13) & 0xE0);
        TXMessage[TempPtr].Specific.TXBSIDH.Byte = (unsigned char) ((Message.Address >> 21) & 0xFF);
        TXMessage[TempPtr].Specific.TXBSIDL.Bits.EXIDE = 1;
    } else //Standard identifier
    {
        TXMessage[TempPtr].Specific.TXBSIDL.Byte = (unsigned char) ((Message.Address << 5) & 0xFF); //Put address <2:0> in SIDL
        TXMessage[TempPtr].Specific.TXBSIDH.Byte = (unsigned char) ((Message.Address >> 3) & 0xFF); //Put address <10:3> in SIDH
        TXMessage[TempPtr].Specific.TXBSIDL.Bits.EXIDE = 0;
    }
    TXMessage[TempPtr].Specific.TXBCON.Byte = Message.Priority & 0x03; //Set the internal priority of the data frame

    TXWPtr = TempPtr;

    //Reenable an interrupt if it is idle, it doesn't matter if another TX source caught the pending message
    //before the source that is enabled here does since the interrupt CHECKS if there are any messages pending
    //and if the buffer is empty, it will disable itself again.
    if (Message.Ext) {
        if ((TXBIE & 0x1C) != 0x1c) {
            PIR3bits.TXB2IF = 1;
            return 0;
        }
#if (ECAN_B0_TXRX_MODE_VAL == ECAN_BUFFER_TX)
        else if (!BIE0bits.B0IE) {
            PIR3bits.TXB2IF = 1;
            return 0;
        }
#endif
#if (ECAN_B1_TXRX_MODE_VAL == ECAN_BUFFER_TX)
        else if (!BIE0bits.B1IE) {
            PIR3bits.TXB2IF = 1;
            return 0;
        }
#endif
#if (ECAN_B2_TXRX_MODE_VAL == ECAN_BUFFER_TX)
        else if (!BIE0bits.B2IE) {
            PIR3bits.TXB2IF = 1;
            return 0;
        }
#endif
#if (ECAN_B3_TXRX_MODE_VAL == ECAN_BUFFER_TX)
        else if (!BIE0bits.B3IE) {
            PIR3bits.TXB2IF = 1;
            return 0;
        }
#endif
#if (ECAN_B4_TXRX_MODE_VAL == ECAN_BUFFER_TX)
        else if (!BIE0bits.B4IE) {
            PIR3bits.TXB2IF = 1;
            return 0;
        }
#endif
#if (ECAN_B5_TXRX_MODE_VAL == ECAN_BUFFER_TX)
        else if (!BIE0bits.B5IE) {
            PIR3bits.TXB2IF = 1;
            return 0;
        }
#endif
    } else {
        if (!PIE3bits.TXB0IE) { //TXB0 is idle
            PIE3bits.TXB0IE = 1; //enable TXB0 to fire an interrupt to send the pending message
            PIR3bits.TXB0IF = 1;
            return 0;
        } else if (!PIE3bits.TXB1IE) { //else if TXB1 is idle
            PIE3bits.TXB1IE = 1; //enable TXB0 to fire an interrupt to send the pending message
            PIR3bits.TXB1IF = 1;
            return 0;
        } else if (!PIE3bits.TXB2IE) { //finally try TXB2
            PIE3bits.TXB2IE = 1; //enable TXB0 to fire an interrupt to send the pending message
            PIR3bits.TXB2IF = 1;
            return 0;
        }
    }

    return 2;
}

/*********************************************************************
 * Function:        char CANRXMessageIsPending(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          1 -> At least one received message is pending in the RX buffer
 *                  0 -> No received messages are pending
 *
 * Side Effects:    None
 *
 * Overview:        Checks if the RX Write pointer is equal to RX Read pointer and
 *                  if so returns 0, else returns 1
 *
 * Note:            Since no care is taken if the buffer overflow
 *                  this function has to be polled frequently to
 *                  prevent a software receive buffer overflow
 ********************************************************************/
char CANRXMessageIsPending(void) {
    if (RXWPtr != RXRPtr)
        return 1;
    else
        return 0;
}

void resetRXMessages(void) {
    RXWPtr = RXRPtr = 0;
}

/*********************************************************************
 * Function:        struct CANMessage CANGet(void)
 *
 * PreCondition:    An unread message has to be in the buffer
 *                  use RXCANMessageIsPending(void) prior to 
 *                  calling this function in order to determine
 *                  if an unread message is pending.
 *
 * Input:           None
 *
 * Output:          The received message
 *
 * Side Effects:    Will modify the RX Buffer register�s Read pointer
 *
 * Overview:        Pops the the first message of the RX buffer
 *
 * Note:            None
 ********************************************************************/
struct CANMessage CANGet(unsigned char mode) {
    struct CANMessage ReturnMessage;
    unsigned char TempPtr, i;

    /*Do not modify the RX pointer until the message has been successfully
      copied to prevent ISR to overwrite it (Note. this is not implemented in the ISR yet)*/
    if (RXRPtr >= RXBUF - 1) //Check if pointer will overflow
    {
        TempPtr = 0;
    } else {
        TempPtr = RXRPtr + 1;
    }

    ReturnMessage.NoOfBytes = RXMessage[TempPtr].Specific.RXBDLC.Byte & 0x0F; //copy data count
    if (RXMessage[TempPtr].Specific.RXBCON.Bits.RXRTRRO) //Remote frame request
    {
        ReturnMessage.Remote = 1;
    } else //Data frame
    {
        ReturnMessage.Remote = 0; //Clear remote flag
        for (i = 0; i < ReturnMessage.NoOfBytes; i++) //copy data content
            ReturnMessage.Data[i] = RXMessage[TempPtr].Specific.RXBD.Array[i];
    }

    if (mode == ECAN_MODE_0) {
        CANCON = CANCON & 0xF0;
    }



    if (RXMessage[TempPtr].Specific.RXBSIDL.Bits.EXID) //Extended identifier
    {
        ReturnMessage.Ext = 1; //Set the extended identifier flag
        ReturnMessage.Address = (unsigned int) (RXMessage[TempPtr].Specific.RXBSIDH.Byte) << 3; //Load the standard identifier into the address
        ReturnMessage.Address |= (RXMessage[TempPtr].Specific.RXBSIDL.Byte >> 5);
        ReturnMessage.Address = (ReturnMessage.Address << 2) | (RXMessage[TempPtr].Specific.RXBSIDL.Byte & 0x03);
        ReturnMessage.Address = ReturnMessage.Address << 16; //and copy the extended address
        ReturnMessage.Address |= (unsigned int) (RXMessage[TempPtr].Specific.RXBEIDH.Byte) << 8;
        ReturnMessage.Address |= RXMessage[TempPtr].Specific.RXBEIDL.Byte;

    } else //Standard identifier
    {
        ReturnMessage.Address = (unsigned int) (RXMessage[TempPtr].Specific.RXBSIDH.Byte) << 3; //Load the standard identifier into the address
        ReturnMessage.Address |= (RXMessage[TempPtr].Specific.RXBSIDL.Byte >> 5);
        ReturnMessage.Ext = 0; //clear the extended identifier flag
    }

    RXRPtr = TempPtr;
    return ReturnMessage;
}

/*********************************************************************
 * Function:        void ECANSetOperationMode(ECAN_OP_MODE mode)
 *
 * Overview:        Use this function to switch ECAN module into
 *                  given operational mode.
 *
 * PreCondition:    None
 *
 * Input:           mode    - Operation mode code
 *                            must be of type ECAN_OP_MODES
 *
 * Output:          MCU is set to requested mode
 *
 * Side Effects:    None
 *
 * Note:            This is a blocking call.  It will not return until
 *                  requested mode is set.
 ********************************************************************/
void ECANSetOperationMode(ECAN_OP_MODE mode) {
    CANCON &= 0x1F; // clear previous mode
    CANCON |= mode; // set new mode

    while (ECANGetOperationMode() != mode); // Wait till desired mode is set.
}

void clearInterruptEnableBit(unsigned char interruptNumber) {
    switch (interruptNumber) {
        case TXB2Mode1Address:
            TXBIEbits.TX2IE = 0;
            break;
        case TXB1Mode1Address:
            TXBIEbits.TX1IE = 0;
            break;
        case TXB0Mode1Address:
            TXBIEbits.TX0IE = 0;
            break;
        case RXTXB0Mode1Address:
            BIE0bits.B0IE = 0;
            break;
        case RXTXB1Mode1Address:
            BIE0bits.B1IE = 0;
            break;
        case RXTXB2Mode1Address:
            BIE0bits.B2IE = 0;
            break;
        case RXTXB3Mode1Address:
            BIE0bits.B3IE = 0;
            break;
        case RXTXB4Mode1Address:
            BIE0bits.B4IE = 0;
            break;
        case RXTXB5Mode1Address:
            BIE0bits.B5IE = 0;
            break;
    }
}

unsigned char getInterruptAddress(void) {
    if (CANSTAT & TXB2Mode1Interrupt)
        return TXB2Mode1Address;
    if (CANSTAT & TXB1Mode1Interrupt)
        return TXB1Mode1Address;
    if (CANSTAT & TXB0Mode1Interrupt)
        return TXB0Mode1Address;
    if (CANSTAT & RXTXB0Mode1Interrupt)
        return RXTXB0Mode1Address;
    if (CANSTAT & RXTXB1Mode1Interrupt)
        return RXTXB1Mode1Address;
    if (CANSTAT & RXTXB2Mode1Interrupt)
        return RXTXB2Mode1Address;
    if (CANSTAT & RXTXB3Mode1Interrupt)
        return RXTXB3Mode1Address;
    if (CANSTAT & RXTXB4Mode1Interrupt)
        return RXTXB4Mode1Address;
    if (CANSTAT & RXTXB5Mode1Interrupt)
        return RXTXB5Mode1Address;

    return 0;

}

unsigned char getFirstFreeTxBuffer(void) {
    if (!TXBIEbits.TXB0IE) {
        TXBIEbits.TXB0IE = 1;
        return TXB0Mode1Address;
    } else if (!TXBIEbits.TXB1IE) {
        TXBIEbits.TXB1IE = 1;
        return TXB1Mode1Address;
    } else if (!TXBIEbits.TXB2IE) {
        TXBIEbits.TXB2IE = 1;
        return TXB2Mode1Address;
    }
#if (ECAN_B0_TXRX_MODE_VAL == ECAN_BUFFER_TX)
    else if (!BIE0bits.B0IE) {
        BIE0bits.B0IE = 1;
        return RXTXB0Mode1Address;
    }
#endif
#if (ECAN_B1_TXRX_MODE_VAL == ECAN_BUFFER_TX)
    else if (!BIE0bits.B1IE) {
        BIE0bits.B1IE = 1;
        return RXTXB1Mode1Address;
    }
#endif
#if (ECAN_B2_TXRX_MODE_VAL == ECAN_BUFFER_TX)
    else if (!BIE0bits.B2IE) {
        BIE0bits.B2IE = 1;
        return RXTXB2Mode1Address;
    }
#endif
#if (ECAN_B3_TXRX_MODE_VAL == ECAN_BUFFER_TX)
    else if (!BIE0bits.B3IE) {
        BIE0bits.B3IE = 1;
        return RXTXB3Mode1Address;
    }
#endif
#if (ECAN_B4_TXRX_MODE_VAL == ECAN_BUFFER_TX)
    else if (!BIE0bits.B4IE) {
        BIE0bits.B4IE = 1;
        return RXTXB4Mode1Address;
    }
#endif
#if (ECAN_B5_TXRX_MODE_VAL == ECAN_BUFFER_TX)
    else if (!BIE0bits.B5IE) {
        BIE0bits.B5IE = 1;
        return RXTXB5Mode1Address;
    }
#endif
    else
        return 0;
}

void setExtFilter(unsigned char number, unsigned long filter) {
    unsigned char* ptr;
    switch (number) {
        case RXF0:
            ptr = (unsigned char*) & RXF0SIDH;
            break;
        case RXF1:
            ptr = (unsigned char*) & RXF1SIDH;
            break;
        case RXF2:
            ptr = (unsigned char*) & RXF2SIDH;
            break;
        case RXF3:
            ptr = (unsigned char*) & RXF3SIDH;
            break;
        case RXF4:
            ptr = (unsigned char*) & RXF4SIDH;
            break;
        case RXF5:
            ptr = (unsigned char*) & RXF5SIDH;
            break;
        case RXF6:
            ptr = (unsigned char*) & RXF6SIDH;
            break;
        case RXF7:
            ptr = (unsigned char*) & RXF7SIDH;
            break;
        case RXF8:
            ptr = (unsigned char*) & RXF8SIDH;
            break;
        case RXF9:
            ptr = (unsigned char*) & RXF9SIDH;
            break;
        case RXF10:
            ptr = (unsigned char*) & RXF10SIDH;
            break;
        case RXF11:
            ptr = (unsigned char*) & RXF11SIDH;
            break;
        case RXF12:
            ptr = (unsigned char*) & RXF12SIDH;
            break;
        case RXF13:
            ptr = (unsigned char*) & RXF13SIDH;
            break;
        case RXF14:
            ptr = (unsigned char*) & RXF14SIDH;
            break;
        case RXF15:
            ptr = (unsigned char*) & RXF15SIDH;
            break;

    }

    *ptr = (long) filter >> 21L;
    *(++ptr) = (((long) filter >> 13L) & 0xe0) | ((long) (filter >> 16L) & 0x03L) | 0x08;                                             \
    *(++ptr) = (long) filter >> 8L;
    *(++ptr) = filter;
}

void setStdFilter(unsigned char number, unsigned long filter) {
    unsigned char* ptr;
    switch (number) {
        case RXF0:
            ptr = (unsigned char*) & RXF0SIDH;
            break;
        case RXF1:
            ptr = (unsigned char*) & RXF1SIDH;
            break;
        case RXF2:
            ptr = (unsigned char*) & RXF2SIDH;
            break;
        case RXF3:
            ptr = (unsigned char*) & RXF3SIDH;
            break;
        case RXF4:
            ptr = (unsigned char*) & RXF4SIDH;
            break;
        case RXF5:
            ptr = (unsigned char*) & RXF5SIDH;
            break;

    }

    *ptr = (long) filter >> 3L;
    *(++ptr) = (long) filter << 5L;
}

void setExtMask(unsigned char number, unsigned long mask) {
    unsigned char* ptr;
    switch (number) {
        case 0:
            ptr = (unsigned char*) & RXM0SIDH;
            break;
        case 1:
            ptr = (unsigned char*) & RXM1SIDH;
            break;


    }

    *ptr = (long) mask >> 21L;
    *(++ptr) = (((long) mask >> 13L) & 0xe0) | ((long) (mask >> 16L) & 0x03L) | 0x08;
    *(++ptr) = (long) mask >> 8L;
    *(++ptr) = mask;

}

void setStdMask(unsigned char number, unsigned long mask) {
    unsigned char* ptr;
    switch (number) {
        case 0:
            ptr = (unsigned char*) & RXM0SIDH;
            break;
        case 1:
            ptr = (unsigned char*) & RXM1SIDH;
            break;


    }

    *ptr = (long) mask >> 3L;
    *(++ptr) = (long) mask << 5L;

}

void clearAllInterruptEnableBits() {
    TXBIE = 0;
#if (ECAN_B0_TXRX_MODE_VAL == ECAN_BUFFER_TX)
    BIE0bits.B0IE = 0;
#endif
#if (ECAN_B1_TXRX_MODE_VAL == ECAN_BUFFER_TX)
    BIE0bits.B1IE = 0;
#endif
#if (ECAN_B2_TXRX_MODE_VAL == ECAN_BUFFER_TX)
    BIE0bits.B2IE = 0;
#endif
#if (ECAN_B3_TXRX_MODE_VAL == ECAN_BUFFER_TX)
    BIE0bits.B3IE = 0;
#endif
#if (ECAN_B4_TXRX_MODE_VAL == ECAN_BUFFER_TX)
    BIE0bits.B4IE = 0;
#endif
#if (ECAN_B5_TXRX_MODE_VAL == ECAN_BUFFER_TX)
    BIE0bits.B5IE = 0;
#endif

}

void ecanErrorHandler(void) {
    //If CAN error handler is enabled
    //Perform error handling code here
    lastB0CON = B0CON;
    lastB1CON = B1CON;
    lastB2CON = B2CON;
    lastB3CON = B3CON;
    lastB4CON = B4CON;
    lastB5CON = B5CON;
    lastCOMSTAT = COMSTAT;
    lastTXB0CON = TXB0CON;
    lastTXB1CON = TXB1CON;
    lastTXB2CON = TXB2CON;
    lastRXERR = RXERRCNT;
    lastTXERR = TXERRCNT;


    restartECAN();

    /*
    if (COMSTATbits.TXBO) {
//        UART_SendString("\r\nBuss Off");
    } else if (COMSTATbits.TXBP) {
//        UART_SendString("\r\nTransmitter Buss Passive");
    } else if (COMSTATbits.TXWARN) {
//        UART_SendString("\r\nTransmit warning");
    }

    if (COMSTATbits.RXBP) {
//        UART_SendString("\r\nReceiver Buss Passive");
    } else if (COMSTATbits.RXWARN) {
//        UART_SendString("\r\nReceiver warning");
    }

    if (TXB0CONbits.TXABT) {
//        UART_SendString("\r\nMessage was aborted in TXB0");
    }
    if (TXB1CONbits.TXABT) {
//        UART_SendString("\r\nMessage was aborted in TXB1");
    }
    if (TXB2CONbits.TXABT) {
//        UART_SendString("\r\nMessage was aborted in TXB2");
    }
    if (TXB0CONbits.TXLARB) {
//        UART_SendString("\r\nMessage lost arbitration in TXB0");
    }
    if (TXB1CONbits.TXLARB) {
//        UART_SendString("\r\nMessage lost arbitration in TXB1");
    }
    if (TXB2CONbits.TXLARB) {
//        UART_SendString("\r\nMessage lost arbitration in TXB2");
    }
    if (TXB0CONbits.TXERR) {
//        UART_SendString("\r\nError has aoocured during sending from in TXB0");
    }
    if (TXB1CONbits.TXERR) {
//        UART_SendString("\r\nError has aoocured during sending from in TXB1");
    }
    if (TXB2CONbits.TXERR) {
//        UART_SendString("\r\nError has aoocured during sending from in TXB2");
    }
#if (ECAN_B0_TXRX_MODE_VAL == ECAN_BUFFER_TX)
    if (B0CONbits.TXABT) {
//        UART_SendString("\r\nMessage was aborted in B0");
    }
    if (B0CONbits.TXLARB) {
//        UART_SendString("\r\nMessage lost arbitration in B0");
    }
    if (B0CONbits.TXERR) {
//        UART_SendString("\r\nError has aoocured during sending from in B0");
    }
#endif
#if (ECAN_B1_TXRX_MODE_VAL == ECAN_BUFFER_TX)
    if (B1CONbits.TXABT) {
//        UART_SendString("\r\nMessage was aborted in B1");
    }
    if (B1CONbits.TXLARB) {
//        UART_SendString("\r\nMessage lost arbitration in B1");
    }
    if (B1CONbits.TXERR) {
//        UART_SendString("\r\nError has aoocured during sending from in B1");
    }
#endif
#if (ECAN_B2_TXRX_MODE_VAL == ECAN_BUFFER_TX)
    if (B2CONbits.TXABT) {
//        UART_SendString("\r\nMessage was aborted in B2");
    }
    if (B2CONbits.TXLARB) {
//        UART_SendString("\r\nMessage lost arbitration in B2");
    }
    if (B2CONbits.TXERR) {
//        UART_SendString("\r\nError has aoocured during sending from in B2");
    }
#endif
#if (ECAN_B3_TXRX_MODE_VAL == ECAN_BUFFER_TX)
    if (B3CONbits.TXABT) {
//        UART_SendString("\r\nMessage was aborted in B3");
    }
    if (B3CONbits.TXLARB) {
//        UART_SendString("\r\nMessage lost arbitration in B3");
    }
    if (B3CONbits.TXERR) {
//        UART_SendString("\r\nError has aoocured during sending from in B3");
    }
#endif
#if (ECAN_B4_TXRX_MODE_VAL == ECAN_BUFFER_TX)
    if (B4CONbits.TXABT) {
//        UART_SendString("\r\nMessage was aborted in B4");
    }
    if (B4CONbits.TXLARB) {
//        UART_SendString("\r\nMessage lost arbitration in B4");
    }
    if (B4CONbits.TXERR) {
//        UART_SendString("\r\nError has aoocured during sending from in B4");
    }
#endif
#if (ECAN_B5_TXRX_MODE_VAL == ECAN_BUFFER_TX)
    if (B5CONbits.TXABT) {
//        UART_SendString("\r\nMessage was aborted in B5");
    }
    if (B5CONbits.TXLARB) {
//        UART_SendString("\r\nMessage lost arbitration in B5");
    }
    if (B5CONbits.TXERR) {
//        UART_SendString("\r\nError has aoocured during sending from in B5");
    }
#endif
     
     * */
}

void restartECAN() {
    abortAllECANTransmits();

    ECANSetOperationMode(ECAN_OP_MODE_CONFIG);
    clearAllInterruptEnableBits();
    delay_ms(10);
    ECANSetOperationMode(ECAN_INIT_MODE);

}

void abortAllECANTransmits() {
    CANCONbits.ABAT = 1;
}