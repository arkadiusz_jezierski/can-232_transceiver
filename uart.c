
#include "uart.h"
#include "comm_constans.h"
//#include <../sources/common/memset.c>
#include <../sources/memset.c>
//========================================================================================
// Initialize UART

void UART_Init(unsigned long int baudRate) {
    unsigned int n;

    // Configure BaudRate
    BRGH = 1; // Low speed.
    BRG16 = 1; // 16-Bit Baud Rate Generator - SPBRGH and SPBRG

    // Baudrate = Fosc/[16(n+1)]  => n = ((Fosc/Baudrate)>>4) - 1;  n = SPBRGH: SPBRG;
    n = ((Fosc / baudRate) / 4) - 1;

    //    n = 68;
    SPBRG = n;
    SPBRGH = n >> 8;
    // Enable the asyncchronous serial port.
    SYNC = 0; // Asynchronous mode
    SPEN = 1; // Serial port enable.
    TRIS_TX = 0;
    TRIS_RX = 1;
    //Configure for Transmitter mode.
    //#ifdef   UART_ON
    TXEN = 1; // Transmit enable
    //Configure for Receiver mode
    CREN = 1; // Enable the reception
    //Interrupt
    RCIF = 0;
    RCIE = 1; // Reception Interrupt Enable
    RCIP = 0;
    GIE = 1; // Global Interrupt Enable
    PEIE = 1; // Perapheral Interrupt Enable
}
//=====================================================================================

void UART_SendByte(unsigned char a) {
    //    LED_UARTTX_ON;
    while (!TRMT);
    TXREG = a;
    //    LED_UARTTX_OFF;
}
//=====================================================================================

void UART_SendString(const char* str) {
    while (*str)
        UART_SendByte(*str++);
}


//====================================================================================

unsigned char UART_getFrame(struct UART_Buffer_FIFO *fifo) {
    static unsigned char data[BUFFER_UART_SIZE];
    static unsigned char counter = 0;
    static unsigned char length;
    static unsigned char crc = 0;


    if (counter == 0 && RCREG != REQ_FRAME_HDR) {
        crc = 0;
        return 0;
    }
    if (counter < BUFFER_UART_SIZE) {
        data[counter++] = RCREG;
        crc += data[counter - 1];
        if (counter == length && data[length - 1] == REQ_FRAME_FTR) {
            crc -= data[counter - 2];
            if (crc != data[counter - 2]) {
                crc = 0;
                counter = 0;
                length = 0;
                return 1;
            }
            if (++fifo->ptrW >= FIFO_SIZE) {
                fifo->ptrW = 0;
            }
            for (size_t i = 0; i < counter; i++) {
                fifo->uartBuffer[fifo->ptrW].data[i] = data[i];
            }
            fifo->uartBuffer[fifo->ptrW].length = length;

            counter = 0;
            length = 0;
            crc = 0;
            return 0;
        }
    } else {
        length = 0;
        counter = 0;
        crc = 0;
    }
    if (counter == 2) {
        length = data[1];
    }

    return 0;
}

void getBuffer(UART_Buffer_FIFO* fifo, UART_Buffer* buffer) {
    unsigned char tmpPtr;

    if (fifo->ptrR >= FIFO_SIZE - 1) //Check if pointer will overflow
    {
        tmpPtr = 0;
    } else {
        tmpPtr = fifo->ptrR + 1;
    }

    for (size_t i = 0; i < fifo->uartBuffer[tmpPtr].length; i++) {
        buffer->data[i] = fifo->uartBuffer[tmpPtr].data[i];
        buffer->length = fifo->uartBuffer[tmpPtr].length;
    }

    fifo->ptrR = tmpPtr;
}

unsigned char checkUartFIFObuffer(UART_Buffer_FIFO *fifo) {
    if (fifo->ptrR != fifo->ptrW)
        return 1;
    else
        return 0;
}

unsigned long getCanID(struct UART_Buffer *buffer) {

    unsigned long id = 0;

    id = ((unsigned long) buffer->data[OFFSET_UART2CAN_ID]) << 24;
    id |= ((unsigned long) buffer->data[OFFSET_UART2CAN_ID + 1]) << 16;
    id |= ((unsigned long) (buffer->data[OFFSET_UART2CAN_ID + 2]) << 8);
    id |= (unsigned long) (buffer->data[OFFSET_UART2CAN_ID + 3]);

    return id;
}

unsigned char getCanData(struct UART_Buffer *buffer, unsigned char *data) {
    unsigned char length = buffer->data[OFFSET_UART2CAN_LENGTH] - FRAME_UART2CAN_MIN_SIZE;

    for (size_t i = 0; i < length; i++) {
        data[i] = buffer->data[OFFSET_UART2CAN_DATA + i];
    }

    return length;
}

void initUartFIFO(struct UART_Buffer_FIFO *fifo) {
    fifo->ptrW = fifo->ptrR = 0;
    for (size_t f = 0; f < FIFO_SIZE; f++) {
        fifo->uartBuffer[f].length = 0;
        for (size_t i = 0; i < BUFFER_UART_SIZE; i++)
            fifo->uartBuffer[f].data[i] = 0;
    }
}

unsigned char checkCRC(struct UART_Buffer *buffer) {
    unsigned char crc = 0;
    for (unsigned char i = 0; i < buffer->length; i++)
        crc += buffer->data[i];

    return (crc == 0);
}

unsigned char getCommand(UART_Buffer* buffer) {
    return buffer->data[OFFSET_UART2CAN_COMMAND];
}

void sendACK() {
    sendEmptyUartFrame(CMD_RESP_ACK);
}

void sendNACK() {
    sendEmptyUartFrame(CMD_RESP_NACK);
}

void sendCrcError() {
    sendEmptyUartFrame(CMD_RESP_BAD_CRC);

}

unsigned char getFilterNo(UART_Buffer* buffer) {
    return buffer->data[OFFSET_UART2CAN_NO];
}

unsigned long getFilterID(UART_Buffer* buffer) {
    unsigned long id = 0;

    id = ((unsigned long) buffer->data[OFFSET_UART2CAN_FILT_ID]) << 24;
    id |= ((unsigned long) buffer->data[OFFSET_UART2CAN_FILT_ID + 1]) << 16;
    id |= ((unsigned long) (buffer->data[OFFSET_UART2CAN_FILT_ID + 2]) << 8);
    id |= (unsigned long) (buffer->data[OFFSET_UART2CAN_FILT_ID + 3]);

    return id;
}

unsigned char getEcanMode(UART_Buffer* buffer) {
    return buffer->data[OFFSET_UART2CAN_MODE];
}

void sendCanFrame(struct CANMessage *message) {
    UART_Buffer buffer;

    buffer.data[0] = (unsigned char) ((message->Address >> 24) & 0xff);
    buffer.data[1] = (unsigned char) ((message->Address >> 16) & 0xff);
    buffer.data[2] = (unsigned char) ((message->Address >> 8) & 0xff);
    buffer.data[3] = (unsigned char) (message->Address & 0xff);

    for (unsigned char i = 0; i < message->NoOfBytes; i++)
        buffer.data[i + 4] = (message->Data[i]);
    buffer.length = 4 + message->NoOfBytes;
    sendUartFrame(&buffer, CMD_RESP_DATA);
}

void sendHandshake(void) {
    UART_Buffer buffer;
    memcpy((unsigned char *) buffer.data, "CAN232", 6);
    buffer.length = 6;
    sendUartFrame(&buffer, CMD_RESP_HNDSHK);
}

void sendErrorFrame(void) {
    UART_Buffer buffer;
    buffer.length = 0;
    buffer.data[buffer.length++] = lastRXERR;
    buffer.data[buffer.length++] = lastTXERR;
    buffer.data[buffer.length++] = lastCOMSTAT;
    buffer.data[buffer.length++] = (lastTXB0CON & 0x0f) | ((lastTXB1CON << 4) & 0xf0);
    buffer.data[buffer.length++] = (lastTXB2CON & 0x0f);
    buffer.data[buffer.length++] = (lastB4CON & 0x0f) | ((lastB5CON << 4) & 0xf0);

    sendUartFrame(&buffer, CMD_RESP_ERROR);
}

void sendNoFrame(void) {
    UART_Buffer buffer;
    buffer.length = 0;
    buffer.data[buffer.length++] = RXERRCNT;
    buffer.data[buffer.length++] = TXERRCNT;
    buffer.data[buffer.length++] = TXBIE;
    buffer.data[buffer.length++] = BIE0;
    buffer.data[buffer.length++] = (RXRPtr > RXWPtr) ? (RXRPtr - RXWPtr) : (RXWPtr - RXRPtr);
    buffer.data[buffer.length++] = (TXRPtr > TXWPtr) ? (TXRPtr - TXWPtr) : (TXWPtr - TXRPtr);

    sendUartFrame(&buffer, CMD_RESP_NO_DATA);
}

void convertToCanMessage(struct UART_Buffer * uartBuffer, struct CANMessage * canMessage) {
    canMessage->Address = (unsigned long) uartBuffer->data[OFFSET_UART2CAN_ID] << 24;
    canMessage->Address |= (unsigned long) uartBuffer->data[OFFSET_UART2CAN_ID + 1] << 16;
    canMessage->Address |= (unsigned long) uartBuffer->data[OFFSET_UART2CAN_ID + 2] << 8;
    canMessage->Address |= (unsigned long) uartBuffer->data[OFFSET_UART2CAN_ID + 3];
    canMessage->NoOfBytes = uartBuffer->length - FRAME_UART2CAN_MIN_SIZE;
    for (size_t i = 0; i < canMessage->NoOfBytes; i++) {
        canMessage->Data[i] = uartBuffer->data[i + OFFSET_UART2CAN_DATA];
    }
    canMessage->Ext = 1;
    canMessage->Priority = 0;
}

void convertToRegulCanMessage(struct UART_Buffer * uartBuffer, struct CANMessage * canMessage) {
  
    canMessage->Address = (unsigned long) uartBuffer->data[OFFSET_UART2CAN_ID] << 8;
    canMessage->Address |= (unsigned long) uartBuffer->data[OFFSET_UART2CAN_ID + 1];
    canMessage->NoOfBytes = uartBuffer->length + 2 - FRAME_UART2CAN_MIN_SIZE;
    for (size_t i = 0; i < canMessage->NoOfBytes; i++) {
        canMessage->Data[i] = uartBuffer->data[i + OFFSET_UART2CAN_DATA - 2];
    }
    canMessage->Ext = 0;
    canMessage->Priority = 0;

    
}

//[T] [L] [C] [D...] [CRC]

void sendUartFrame(UART_Buffer* buffer, unsigned char command) {
    LED_UARTTX_ON
            unsigned char len = buffer->length + 4;
    unsigned char crc = RESP_FRAME_HDR + len + command;
    for (unsigned char i = 0; i < buffer->length; i++) {
        crc += buffer->data[i];
    }
    UART_SendByte(RESP_FRAME_HDR);
    UART_SendByte(len);
    UART_SendByte(command);
    for (unsigned char i = 0; i < buffer->length; i++) {
        UART_SendByte(buffer->data[i]);
    }
    UART_SendByte(crc);
    LED_UARTTX_OFF
}

void sendEmptyUartFrame(unsigned char command) {
    LED_UARTTX_ON
            unsigned char crc = RESP_FRAME_HDR + 4 + command;
    UART_SendByte(RESP_FRAME_HDR);
    UART_SendByte(4);
    UART_SendByte(command);
    UART_SendByte(crc);
    LED_UARTTX_OFF
}