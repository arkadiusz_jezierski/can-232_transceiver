// UART.h
/*
        Author: Ngo Hai Bac
        Website: www.ngohaibac.com
 */
//=======================================================================================
#ifndef _UART_H
#define _UART_H
//========================================================================================

#include <pic18.h>
#include <stdio.h>
#include <stdlib.h>
#include "can.h"
#include "board.h"
#include "comm_constans.h"


#define	Fosc	32000000



#define TX		RC6
#define RX 		RC7
#define TRIS_TX		TRISC6
#define TRIS_RX		TRISC7

#define	UART_Data	TMR1H	// UART Data temporary

#define FIFO_SIZE            5
#define BUFFER_UART_SIZE     20


typedef struct UART_Buffer {
    unsigned char data[BUFFER_UART_SIZE];
    unsigned char length;
} UART_Buffer;

typedef struct UART_Buffer_FIFO{
    UART_Buffer uartBuffer[FIFO_SIZE];
    unsigned char ptrW;
    unsigned char ptrR;
} UART_Buffer_FIFO;


//========================================================================================
// Declare sosme functions
void UART_Init(unsigned long int baudRate); // Initialize for UART
void UART_SendByte(unsigned char a);
void UART_SendString(const char* str);
unsigned char UART_getFrame(struct UART_Buffer_FIFO *fifo);
void getBuffer(struct UART_Buffer_FIFO *fifo, struct UART_Buffer *buffer);
void initUartFIFO(struct UART_Buffer_FIFO *fifo);
unsigned char checkUartFIFObuffer(UART_Buffer_FIFO *fifo);

unsigned long  getCanID(struct UART_Buffer *buffer);
unsigned char getCanData(struct UART_Buffer *buffer, unsigned char *data);
unsigned char checkCRC(struct UART_Buffer *buffer);
unsigned char getCommand(struct UART_Buffer *buffer);
unsigned char getFilterNo(struct UART_Buffer *buffer);
unsigned long  getFilterID(struct UART_Buffer *buffer);
unsigned char  getEcanMode(struct UART_Buffer *buffer);

void sendACK(void);
void sendNACK(void);
void sendNoFrame(void);
void sendCanFrame(struct CANMessage *message);
void sendErrorFrame(void);
void sendHandshake(void);
void sendCrcError(void);

void convertToCanMessage (struct UART_Buffer * uartBuffer,struct CANMessage * canMessage);
void convertToRegulCanMessage (struct UART_Buffer * uartBuffer,struct CANMessage * canMessage);

void sendUartFrame(struct UART_Buffer *buffer, unsigned char command);
void sendEmptyUartFrame(unsigned char command);
#endif
