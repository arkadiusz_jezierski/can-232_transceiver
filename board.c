#include "board.h"

void initPortIO() {
    LED_CANERR_DIR &= ~((1 << LED_CANERR_ACT) | (1 << LED_CANERR_PASS) | (1 << LED_CANERR_OFF));
    LED_CAN_DIR &= ~((1 << LED_CAN_RX) | (1 << LED_CAN_TX));
    LED_UART_DIR &= ~((1 << LED_UART_RX) | (1 << LED_UART_TX));
    LED_FUN_DIR &= ~((1 << LED_FUN_1) | (1 << LED_FUN_2) | (1 << LED_FUN_3));
    testPortIO();
}

void testPortIO() {
    LED_CANERR_ACT_ON;
    LED_CANERR_OFF_ON;
    LED_CANERR_PASS_ON;
    LED_CANRX_ON;
    LED_CANTX_ON;
    LED_UARTRX_ON;
    LED_UARTTX_ON;
    LED_FUN_1_ON;
    LED_FUN_2_ON;
    LED_FUN_3_ON;

    delay_ms(1000);
    LED_CANERR_ACT_OFF;
    LED_CANERR_OFF_OFF;
    LED_CANERR_PASS_OFF;
    LED_CANRX_OFF;
    LED_CANTX_OFF;
    LED_UARTRX_OFF;
    LED_UARTTX_OFF;
    LED_FUN_1_OFF;
    LED_FUN_2_OFF;
    LED_FUN_3_OFF;

}

void setCanErrorLeds() {
    static unsigned int sw = 0;
    static unsigned char transmit = 1;
    sw++;
    if (sw == 0) {
        if (transmit) {
            transmit = 0;
            LED_CANERR_ACT_ON;
            if (TXBO) {
                LED_CANERR_OFF_ON;
                LED_CANERR_PASS_ON;
            } else if (TXBP) {
                LED_CANERR_OFF_ON;
                LED_CANERR_PASS_OFF;
            } else if (TXWARN) {
                LED_CANERR_OFF_OFF;
                LED_CANERR_PASS_ON;
            } else {
                LED_CANERR_OFF_OFF;
                LED_CANERR_PASS_OFF;
            }

        } else {
            transmit = 1;
            LED_CANERR_ACT_OFF;

            if (RXBP) {
                LED_CANERR_OFF_ON;
                LED_CANERR_PASS_ON;
            } else if (RXWARN) {
                LED_CANERR_OFF_OFF;
                LED_CANERR_PASS_ON;
            } else {
                LED_CANERR_OFF_OFF;
                LED_CANERR_PASS_OFF;
            }

        }
    }
}