#include  <pic18.h>
#include "can.h"
#include "uart.h"
#include "board.h"
#include "comm_constans.h"


#pragma config CONFIG1H = 0x6
#pragma config CONFIG2H = 0x1E
#pragma config CONFIG3H = 0x80      //PORTB <0:3> - set as I/O
#pragma config CONFIG4L = 0x91      //PORTB <0:3> - set as I/O



UART_Buffer_FIFO uartFIFO;

volatile unsigned char errorOccured = 0;
volatile unsigned char badCrcOccured = 0;
unsigned char errorTransmitPending = 0;
unsigned char mode = ECAN_MODE_1;

void main() //Main entry
{

    initPortIO();
    UART_Init(115200);
    initUartFIFO(&uartFIFO);


    struct CANMessage RX_Message, TX_Message; //Declare one message structure for transmission and one for reception

    ECANInitialize(mode);
    setExtFilter(0, 0);
    setExtMask(0, 0);

    GIE = 1;
    PEIE = 1;
    IPEN = 0;

    while (1) {


        if (checkUartFIFObuffer(&uartFIFO)) {

            UART_Buffer uartBuffer;
            getBuffer(&uartFIFO, &uartBuffer);


            //            UART_SendString("R");
            //            UART_SendByte(uartFIFO.ptrR);
            //            UART_SendString("W");
            //            UART_SendByte(uartFIFO.ptrW);
            //            UART_SendString("L");
            //            UART_SendByte(uartBuffer.length);
            //
            //            for (int i = 0; i < uartBuffer.length; i++){
            //                UART_SendByte(uartBuffer.data[i]);
            //            }

            switch (getCommand(&uartBuffer)) {
                case CMD_REQ_INIT: //  [?] [6] [i] [m] [Crc] [CR]
                    mode = (getEcanMode(&uartBuffer)) ? ECAN_MODE_1 : ECAN_MODE_0;
                    sendACK();
                    ECANInitialize(mode);
                    break;
                case CMD_REQ_RESET: //  [?] [5] [r] [Crc] [CR]
                    restartECAN();
                    sendACK();
                    break;
                case CMD_REQ_HNDSHK: //  [?] [5] [h] [Crc] [CR]
                    sendHandshake();
                    break;
                case CMD_REQ_DELETE: //  [?] [5] [d] [Crc] [CR]
                    resetRXMessages();
                    sendACK();
                    break;
                case CMD_REQ_FILT: //  [?] [10] [f] [No] [Id] [Id] [Id] [Id] [Crc] [CR]
                    if (mode == ECAN_MODE_0) {
                        setStdFilter(getFilterNo(&uartBuffer), getFilterID(&uartBuffer));
                    } else {
                        setExtFilter(getFilterNo(&uartBuffer), getFilterID(&uartBuffer));
                    }
                    sendACK();
                    break;
                case CMD_REQ_MASK: //  [?] [10] [m] [No] [Id] [Id] [Id] [Id] [Crc] [CR]
                    if (mode == ECAN_MODE_0) {
                        setStdMask(getFilterNo(&uartBuffer), getFilterID(&uartBuffer));
                    } else {
                        setExtMask(getFilterNo(&uartBuffer), getFilterID(&uartBuffer));
                    }
                    sendACK();
                    break;
                case CMD_REQ_GET: //  [?] [5] [g] [CRC] [CR]
                    if (errorTransmitPending) {
                        if (mode == ECAN_MODE_1) {
                            errorTransmitPending = 0;
                            sendErrorFrame();
                        }
                    } else if (CANRXMessageIsPending()) { //Check if there is an unread CAN message
                        RX_Message = CANGet(mode); //Get the message
                        sendCanFrame(&RX_Message);
                    } else {
                        sendNoFrame();
                    }
                    break;
                case CMD_REQ_TRANS: //  [?] [Len] [t] [Id] [Id] [Id] [Id] [Dat] ... [Dat] [CRC] [CR]
                    convertToCanMessage(&uartBuffer, &TX_Message);
                    if (CANPutToFIFO(TX_Message)) {
                        sendNACK();
                    } else {
                        sendACK();
                    }
                    break; // 0x3f 0xf 0x79 0x3 0x30 0x0 0x0 0x0 0x0 0x1d 0x5 0x0 0x0 0x29 0xd
                case CMD_REQ_TRANS_REG: //  [?] [Len] [y] [Id] [Id] [Dat] ... [Dat] [CRC] [CR]
                    convertToRegulCanMessage(&uartBuffer, &TX_Message);
                    if (CANPutToFIFO(TX_Message)) {
                        sendNACK();
                    } else {
                        sendACK();
                    }
                    break;
            }



        }

        if (badCrcOccured) {
            badCrcOccured = 0;
            sendCrcError();
        }

        if (errorOccured) {
            errorOccured = 0;
            errorTransmitPending = 1;
            ecanErrorHandler();

        }

        setCanErrorLeds();

    }
}

void interrupt ISR(void) {
    unsigned char TempCANCON;


    if (PIR3 & PIE3) {
        if (mode == ECAN_MODE_0) {
            TempCANCON = CANCON;
            if (PIR3bits.RXB0IF && PIE3bits.RXB0IE) {
                RXB0IF = 0; //Clear interrupt flag
                CANCON = CANCON & 0xF0 | RXB0Interrupt;
                CANGetMessage();
            } else if (PIR3bits.RXB1IF && PIE3bits.RXB1IE) {
                RXB1IF = 0; //Clear interrupt flag
                CANCON = CANCON & 0xF0 | RXB1Interrupt;
                CANGetMessage();
            } else if (PIR3bits.TXB0IF && PIE3bits.TXB0IE) {
                CANCON = CANCON & 0xF0 | TXB0Interrupt;
                if (CANPutMessage())
                    PIE3bits.TXB0IE = 0;
                else
                    PIR3bits.TXB0IF = 0;
            } else if (PIR3bits.TXB1IF && PIE3bits.TXB1IE) {
                CANCON = CANCON & 0xF0 | TXB1Interrupt;
                if (CANPutMessage())
                    PIE3bits.TXB1IE = 0;
                else
                    PIR3bits.TXB1IF = 0;
            } else if (PIR3bits.TXB2IF && PIE3bits.TXB2IE) {
                CANCON = CANCON & 0xF0 | TXB2Interrupt;
                if (CANPutMessage()) //if there wasn't any more messages to send
                    PIE3bits.TXB2IE = 0; //disable interrupts for TXB2 and leave TXB2IF
                else //still on so PutCAN() can reenable the interrupt and instantly vector to ISR
                    PIR3bits.TXB2IF = 0; //message was sent, just clear the interrupt flag.
            } else if (ERRIF && ERRIE) {
                ERRIF = 0; //Clear interrupt flag
                errorOccured = 1;
                //No error handler implemented!!!
            } else if (WAKIF && WAKIE) {
                WAKIF = 0; //Clear interrupt flag
            }
            CANCON = TempCANCON;
        } else {

            TempCANCON = ECANCON;

            if (PIR3bits.RXB1IF && PIE3bits.RXB1IE) {
                LED_CANRX_ON
                ECANCON = (ECANCON & 0xE0) | (CANSTAT & 0x1F);
                CANGetMessage();
                RXB1IF = 0; //Clear interrupt flag
                LED_CANRX_OFF;
            } else if (PIR3bits.TXB2IF && PIE3bits.TXB2IE) {
                LED_CANTX_ON;
                unsigned char interruptBufferAddress = getInterruptAddress();
                if (interruptBufferAddress) {
                    clearInterruptEnableBit(interruptBufferAddress);
                }
                interruptBufferAddress = getFirstFreeTxBuffer();
                if (interruptBufferAddress) {
                    ECANCON = (ECANCON & 0xE0) | interruptBufferAddress;
                    if (CANPutMessage()) { //if there wasn't any more messages to send
                        clearInterruptEnableBit(interruptBufferAddress);
                    }
                }

                PIR3bits.TXB2IF = 0;
                LED_CANTX_OFF;
            } else if (ERRIF && ERRIE) {
                ERRIF = 0; //Clear interrupt flag

                errorOccured = 1;
            } else if (WAKIF && WAKIE) {
                WAKIF = 0; //Clear interrupt flag
            }
            ECANCON = TempCANCON;
        }
    }

    if (PIR1bits.RCIF && PIE1bits.RCIE) {
        LED_UARTRX_ON
        badCrcOccured = UART_getFrame(&uartFIFO);
        LED_UARTRX_OFF
    }


}



